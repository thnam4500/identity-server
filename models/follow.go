package models

type followStatus int32

const FollowingStatus followStatus = 1
const UnfollowStatus followStatus = 0

type Follow struct {
	Id              int64        `gorm:"column:id"`
	UserId          int64        `gorm:"column:user_id;foreignkey:id"`
	FollowingUserId int64        `gorm:"column:following_user_id"`
	Status          followStatus `gorm:"column:status"`
	CreatedAt       int64        `gorm:"column:created_at"`
	UpdatedAt       int64        `gorm:"column:updated_at"`
}

func (Follow) TableName() string {
	return "follows"
}
