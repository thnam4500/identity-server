package models

import "github.com/thnam4500/identity/pb"

type GetFollowUser struct {
	Id             int64  `gorm:"column:id"`
	Username       string `gorm:"column:username"`
	Email          string `gorm:"column:email"`
	HashedPassword string `gorm:"column:hashed_password"`
	DateOfBirth    int64  `gorm:"column:date_of_birth"`
	FirstName      string `gorm:"column:first_name"`
	LastName       string `gorm:"column:last_name"`
	Role           int32  `gorm:"column:role"`
	CreatedAt      int64  `gorm:"column:created_at"`
	UpdatedAt      int64  `gorm:"column:updated_at"`
}

func (g *GetFollowUser) ToGetUserProtoResponse() *pb.UserData {
	return &pb.UserData{
		Id:          g.Id,
		Username:    g.Username,
		Email:       g.Email,
		DateOfBirth: g.DateOfBirth,
		FirstName:   g.FirstName,
		LastName:    g.LastName,
		Role:        g.Role,
		CreatedAt:   g.CreatedAt,
		UpdatedAt:   g.UpdatedAt,
	}
}
