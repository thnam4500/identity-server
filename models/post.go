package models

type VisibleStatus int32

const OwnerVisibleStatus VisibleStatus = 1
const FollowerVisibleStatus VisibleStatus = 2
const PublicVisibleStatus VisibleStatus = 3

type Post struct {
	Id           int64         `gorm:"column:id"`
	UserId       int64         `gorm:"column:user_id"`
	Content      string        `gorm:"column:content_text"`
	ContentImage string        `gorm:"column:content_image_path"`
	Visible      VisibleStatus `gorm:"column:visible"`
	CreatedAt    int64         `gorm:"column:created_at"`
	UpdatedAt    int64         `gorm:"column:updated_at"`
}
