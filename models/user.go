package models

type RoleType int32

const UserRole RoleType = 0
const AuthorRole RoleType = 1
const AdminRole RoleType = 2

type User struct {
	Id             int64  `gorm:"column:id"`
	Username       string `gorm:"column:username"`
	Email          string `gorm:"column:email"`
	HashedPassword string `gorm:"column:hashed_password"`
	DateOfBirth    int64  `gorm:"column:date_of_birth"`
	FirstName      string `gorm:"column:first_name"`
	LastName       string `gorm:"column:last_name"`
	Role           int32  `gorm:"column:role"`
	CreatedAt      int64  `gorm:"column:created_at"`
	UpdatedAt      int64  `gorm:"column:updated_at"`
}

func (User) TableName() string {
	return "users"
}
