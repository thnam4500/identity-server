generate:
	@rm -rf pb
	@protoc --go_out=. --go_opt=module=github.com/thnam4500/identity \
    --go-grpc_out=. --go-grpc_opt=module=github.com/thnam4500/identity \
    proto/*.proto
docker-run:
	@docker run --name postgres \
	-p 5432:5432 \
	-e POSTGRES_USERNAME=postgres \
	-e POSTGRES_PASSWORD=password \
	-d postgres