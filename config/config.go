package config

import (
	"bytes"
	"fmt"

	"github.com/spf13/viper"
)

var configDefault = []byte(`
redis:
  address: localhost
  pass: 
  port: 6379

postgres:
  address: localhost
  user: postgres
  pass: password
  port: 5432
  dbname: identity
`)

type Config struct {
	Redis    *RedisConnection    `mapstructure:"redis"`
	Postgres *PostgresConnection `mapstructure:"postgres"`
}

type RedisConnection struct {
	Address string `mapstructure:"address"`
	Pass    string `mapstructure:"pass"`
	Port    string `mapstructure:"port"`
}

type PostgresConnection struct {
	Adress string `mapstructure:"address"`
	User   string `mapstructure:"user"`
	Pass   string `mapstructure:"pass"`
	DBName string `mapstructure:"dbname"`
	Port   string `mapstructure:"port"`
}

func Load() *Config {
	viper.SetConfigName("env")  // name of config file (without extension)
	viper.SetConfigType("yaml") // REQUIRED if the config file does not have the extension in the name
	viper.AddConfigPath(".")    // optionally look for config in the working directory
	if err := viper.ReadConfig(bytes.NewReader(configDefault)); err != nil {
		panic(fmt.Errorf("fatal error load default config: %w", err))
	}
	_ = viper.MergeInConfig() // Find and read the config file
	var config Config
	if err := viper.Unmarshal(&config); err != nil {
		panic(fmt.Errorf("fatal error Unmarshal config file: %w", err))
	}
	return &config
}
