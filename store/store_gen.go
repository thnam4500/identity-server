package store

import (
	"github.com/thnam4500/identity/models"
	"gorm.io/gorm"
)

type GetUserParams struct {
	Email    string
	Username string
}

type Store interface {
	GetUserByCondition(params GetUserParams) (*models.User, bool, error)
	CreateUser(user *models.User) error
	GetFollowingUser(userId int64, page, limit int32) ([]*models.GetFollowUser, error)
	GetFollowedUser(userId int64, page, limit int32) ([]*models.GetFollowUser, error)
	FollowUser(userId int64, followId int64) error
	UnFollowUser(userId int64, unfollowId int64) error
	CheckFollower(userId int64, followerId int64) (*models.Follow, bool, error)
	GetUserPosts(userId int64, visibleStatus int32, page, limit int32) ([]*models.Post, error)
	CreatePost(post *models.Post) error
	SetVisiblePost(userId int64, postId int64, visible models.VisibleStatus) error
	GetUserPost(postId int64) (*models.Post, bool, error)
	DeletePost(userId int64, postId int64) error
	EditPost(postId int64, editData *models.Post) error
}

type store struct {
	db *gorm.DB
}

func NewStore(db *gorm.DB) Store {
	return &store{db: db}
}

func (s *store) GetUserByCondition(params GetUserParams) (*models.User, bool, error) {
	return s.getUserByCondition(params)
}
func (s *store) CreateUser(user *models.User) error {
	return s.createUser(user)
}
func (s *store) GetFollowingUser(userId int64, page, limit int32) ([]*models.GetFollowUser, error) {
	return s.getFollowingUser(userId, page, limit)
}

func (s *store) GetFollowedUser(userId int64, page, limit int32) ([]*models.GetFollowUser, error) {
	return s.getFollowedUser(userId, page, limit)
}
func (s *store) FollowUser(userId int64, followId int64) error {
	return s.followUser(userId, followId)
}
func (s *store) UnFollowUser(userId int64, unfollowId int64) error {
	return s.unFollowUser(userId, unfollowId)
}

func (s *store) GetUserPosts(userId int64, visibleStatus int32, page int32, limit int32) ([]*models.Post, error) {
	return s.getUserPosts(userId, visibleStatus, page, limit)
}

func (s *store) GetUserPost(postId int64) (*models.Post, bool, error) {
	return s.getUserPost(postId)
}
func (s *store) CreatePost(post *models.Post) error {
	return s.createPost(post)
}
func (s *store) SetVisiblePost(userId int64, postId int64, visible models.VisibleStatus) error {
	return s.setVisiblePost(userId, postId, visible)
}
func (s *store) EditPost(postId int64, editData *models.Post) error {
	return s.editPost(postId, editData)
}
func (s *store) DeletePost(userId int64, postId int64) error {
	return s.deletePost(userId, postId)
}
func (s *store) CheckFollower(userId int64, followerId int64) (*models.Follow, bool, error) {
	return s.checkFollower(userId, followerId)
}
