package store

import (
	"github.com/thnam4500/identity/models"
	"gorm.io/gorm"
)

func (s *store) getUserPosts(userId int64, visibleStatus int32, page, limit int32) ([]*models.Post, error) {
	var result []*models.Post
	err := s.db.Where("user_id = ? and visible >= ?", userId, visibleStatus).Offset(int((page - 1) * limit)).Limit(int(limit)).Find(&result).Error
	return result, err
}

func (s *store) getUserPost(postId int64) (*models.Post, bool, error) {
	var result models.Post
	err := s.db.Where("id = ?", postId).First(&result).Error
	if err == gorm.ErrRecordNotFound {
		return nil, false, nil
	}
	return &result, true, err
}
func (s *store) createPost(post *models.Post) error {
	return s.db.Create(post).Error
}
func (s *store) setVisiblePost(userId int64, postId int64, visible models.VisibleStatus) error {
	return s.db.Where("user_id = ? and id = ?", userId, postId).Updates(map[string]interface{}{"visible": visible}).Error
}

func (s *store) deletePost(userId int64, postId int64) error {
	return s.db.Where("user_id = ? and id = ?", userId, postId).Delete(&models.Post{}).Error
}

func (s *store) editPost(postId int64, editData *models.Post) error {
	return s.db.Where("id = ?", postId).Updates(editData).Error
}
