package store

import (
	"github.com/thnam4500/identity/models"
	"gorm.io/gorm"
)

func (s *store) getUserByCondition(params GetUserParams) (*models.User, bool, error) {
	query := s.db.Model(&models.User{})
	if len(params.Username) > 0 {
		query = query.Where("username = ?", params.Username)
	}
	if len(params.Email) > 0 {
		query = query.Where("email = ?", params.Email)
	}
	var user *models.User
	err := query.First(&user).Error
	if err == gorm.ErrRecordNotFound {
		return nil, false, nil
	}
	if err != nil {
		return nil, false, err
	}
	return user, true, nil
}

func (s *store) createUser(user *models.User) error {
	return s.db.Model(&models.User{}).Create(&user).Error
}
