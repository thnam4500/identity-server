package store

import (
	"github.com/thnam4500/identity/models"
	"gorm.io/gorm"
)

func (s *store) getFollowingUser(userId int64, page, limit int32) ([]*models.GetFollowUser, error) {
	var user []*models.GetFollowUser
	err := s.db.Table("users u").
		Select("u.*").
		Joins("follow f on u.id = f.user_id").
		Where("user_id = ?", userId).Offset(int((page - 1) * limit)).Limit(int(limit)).Find(&user).Error
	if err != nil {
		return nil, err
	}
	return user, nil
}

func (s *store) getFollowedUser(userId int64, page, limit int32) ([]*models.GetFollowUser, error) {
	var user []*models.GetFollowUser
	err := s.db.Table("users u").
		Select("u.*").
		Joins("follow f on u.id = f.user_id").
		Where("user_id = ?", userId).Offset(int((page - 1) * limit)).Limit(int(limit)).Find(&user).Error
	if err != nil {
		return nil, err
	}
	return user, nil
}

func (s *store) followUser(userId int64, followId int64) error {
	follow := &models.Follow{
		Id:              userId,
		UserId:          userId,
		FollowingUserId: followId,
		Status:          models.FollowingStatus,
		CreatedAt:       0,
		UpdatedAt:       0,
	}
	err := s.db.Where("user_id = ? and following_user_id = ?", userId, followId).FirstOrCreate(&follow).Error
	return err
}
func (s *store) unFollowUser(userId int64, unfollowId int64) error {
	return s.db.Where("user_id = ? and following_user_id = ?", userId, unfollowId).Updates(map[string]interface{}{"status": models.UnfollowStatus}).Error
}
func (s *store) checkFollower(userId int64, followerId int64) (*models.Follow, bool, error) {
	var result models.Follow
	err := s.db.Where("user_id = ? and following_user_id = ?", userId, followerId).First(&result).Error
	if err == gorm.ErrRecordNotFound {
		return nil, false, nil
	}
	return &result, true, err
}
