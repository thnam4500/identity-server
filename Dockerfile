FROM golang:1.20.7-alpine3.18 as builder
RUN apk add --no-cache --update bash git

WORKDIR /app
RUN apk update && apk upgrade && \
    apk add bash git
# pre-copy/cache go.mod for pre-downloading dependencies and only redownloading them in subsequent builds if they change
COPY ./go.mod ./go.sum ./
RUN go mod download

COPY ./ ./
RUN go build -o main /app

FROM alpine:3.18.3

COPY --from=builder /app/main /app/main

WORKDIR /app
CMD ["/app/main"]
