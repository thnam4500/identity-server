package main

import (
	"fmt"
	"net"

	"github.com/redis/go-redis/v9"
	"github.com/thnam4500/identity/config"
	"github.com/thnam4500/identity/gapi"
	"github.com/thnam4500/identity/pb"
	"google.golang.org/grpc"
	"gorm.io/driver/postgres"
	"gorm.io/gorm"
)

type PingRequest struct {
	Username int `json:"user_name"`
}

var rdb *redis.Client

func main() {
	conf := config.Load()
	rdb = redis.NewClient(&redis.Options{
		Addr:     fmt.Sprintf("%s:%s", conf.Redis.Address, conf.Redis.Port),
		Password: conf.Redis.Pass, // no password set
		DB:       0,               // use default DB
	})
	defer rdb.Close()
	conn, err := net.Listen("tcp", ":9000")
	if err != nil {
		panic(err)
	}
	defer conn.Close()
	server := grpc.NewServer()

	dsn := "host=%s user=%s password=%s dbname=%s port=%s sslmode=disable TimeZone=Asia/Ho_Chi_Minh"
	dsn = fmt.Sprintf(dsn, conf.Postgres.Adress, conf.Postgres.User, conf.Postgres.Pass, conf.Postgres.DBName, conf.Postgres.Port)
	db, err := gorm.Open(postgres.Open(dsn), &gorm.Config{})

	s := gapi.NewServer(rdb, db)
	pb.RegisterIdentityServer(server, s)
	fmt.Println("Server is running on port 9000")
	if err := server.Serve(conn); err != nil {
		panic(err)
	}

}
