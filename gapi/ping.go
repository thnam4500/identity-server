package gapi

import (
	"context"

	"github.com/thnam4500/identity/constants"
	"github.com/thnam4500/identity/pb"
)

func (s *Server) Ping(ctx context.Context, in *pb.PingRequest) (*pb.PingResponse, error) {
	return &pb.PingResponse{
		Status:  constants.STATUS_RESPONSE_OK,
		Message: "Pong",
	}, nil
}
