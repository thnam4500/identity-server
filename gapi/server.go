package gapi

import (
	"github.com/redis/go-redis/v9"
	"github.com/thnam4500/identity/pb"
	"github.com/thnam4500/identity/store"
	"gorm.io/gorm"
)

type Server struct {
	redisDatabase *redis.Client
	pb.UnimplementedIdentityServer
	store store.Store
}

func NewServer(rdb *redis.Client, db *gorm.DB) *Server {
	server := &Server{redisDatabase: rdb}
	server.store = store.NewStore(db)
	return server
}
