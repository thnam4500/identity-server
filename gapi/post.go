package gapi

import (
	"context"

	"github.com/thnam4500/identity/constants"
	"github.com/thnam4500/identity/models"
	"github.com/thnam4500/identity/pb"
)

func (s *Server) GetUserPost(ctx context.Context, req *pb.GetUserPostRequest) (*pb.GetUserPostResponse, error) {
	post, exist, err := s.store.GetUserPost(req.PostId)
	if err != nil {
		return &pb.GetUserPostResponse{
			Status:  constants.STATUS_RESPONSE_NOT_OK,
			Message: "failed to get post",
		}, nil
	}
	if !exist {
		return &pb.GetUserPostResponse{
			Status:  constants.STATUS_RESPONSE_OK,
			Message: "invalid post id",
		}, nil
	}
	if post.Visible == models.FollowerVisibleStatus {
		_, exist, err := s.store.CheckFollower(post.UserId, req.UserId)
		if err != nil {
			return &pb.GetUserPostResponse{
				Status:  constants.STATUS_RESPONSE_NOT_OK,
				Message: "error when checking follower",
			}, nil
		}
		if !exist {
			return &pb.GetUserPostResponse{
				Status:  constants.STATUS_RESPONSE_OK,
				Message: "unanuthorized to view this post",
			}, nil
		}
		return &pb.GetUserPostResponse{
			Status:  constants.STATUS_RESPONSE_OK,
			Message: "success",
			Data:    &pb.UserPost{},
		}, nil
	}
	if post.Visible == models.OwnerVisibleStatus {
		if req.UserId == post.UserId {
			return &pb.GetUserPostResponse{
				Status:  constants.STATUS_RESPONSE_OK,
				Message: "success",
				Data:    &pb.UserPost{},
			}, nil
		}
		return &pb.GetUserPostResponse{
			Status:  constants.STATUS_RESPONSE_OK,
			Message: "unanuthorized to view this post",
		}, nil
	}

	return &pb.GetUserPostResponse{
		Status:  constants.STATUS_RESPONSE_OK,
		Message: "success",
		Data: &pb.UserPost{
			Id:               post.Id,
			ContentText:      post.Content,
			ContentImagePath: post.ContentImage,
			Visible:          int32(post.Visible),
			CreatedAt:        post.CreatedAt,
			UpdatedAt:        post.UpdatedAt,
		},
	}, nil
}
func (s *Server) GetUserPosts(ctx context.Context, in *pb.GetUserPostsRequest) (*pb.GetUserPostsResponse, error) {

	posts, err := s.store.GetUserPosts(in.UserId, int32(models.OwnerVisibleStatus), in.Page, in.Limit)
	if err != nil {
		return &pb.GetUserPostsResponse{
			Status:  constants.STATUS_RESPONSE_OK,
			Message: "fail to get posts",
		}, nil

	}
	data := []*pb.UserPost{}
	for _, v := range posts {
		data = append(data, &pb.UserPost{
			Id:               v.Id,
			ContentText:      v.Content,
			ContentImagePath: v.ContentImage,
			Visible:          int32(v.Visible),
			CreatedAt:        v.CreatedAt,
			UpdatedAt:        v.UpdatedAt,
		})
	}
	return &pb.GetUserPostsResponse{
		Status:  constants.STATUS_RESPONSE_OK,
		Message: "success",
		Data:    data,
	}, nil
}
func (s *Server) EditPost(ctx context.Context, in *pb.EditPostRequest) (*pb.EditPostResponse, error) {
	post, exist, err := s.store.GetUserPost(in.PostId)
	if err != nil {
		return &pb.EditPostResponse{
			Status:  constants.STATUS_RESPONSE_NOT_OK,
			Message: "fail to get post",
		}, nil
	}
	if !exist {
		return &pb.EditPostResponse{
			Status:  constants.STATUS_RESPONSE_OK,
			Message: "invalid post id",
		}, nil
	}
	if post.UserId != in.UserId {
		return &pb.EditPostResponse{
			Status:  constants.STATUS_RESPONSE_OK,
			Message: "unauthorized to edit this post",
		}, err
	}
	s.store.EditPost(in.PostId, &models.Post{
		Content:      in.Content,
		ContentImage: in.ContentImagePath,
	})
	return &pb.EditPostResponse{
		Status:  constants.STATUS_RESPONSE_OK,
		Message: "success",
	}, nil
}
func (s *Server) SetVisibleStatusPost(ctx context.Context, in *pb.SetVisibleStatusPostRequest) (*pb.SetVisibleStatusPostResponse, error) {
	post, exist, err := s.store.GetUserPost(in.PostId)
	if err != nil {
		return &pb.SetVisibleStatusPostResponse{
			Status:  constants.STATUS_RESPONSE_NOT_OK,
			Message: "fail to get post",
		}, nil
	}
	if !exist {
		return &pb.SetVisibleStatusPostResponse{
			Status:  constants.STATUS_RESPONSE_OK,
			Message: "invalid post id",
		}, nil
	}
	if post.UserId != in.UserId {
		return &pb.SetVisibleStatusPostResponse{
			Status:  constants.STATUS_RESPONSE_OK,
			Message: "unauthorized to edit this post",
		}, err
	}
	err = s.store.SetVisiblePost(in.UserId, in.PostId, models.VisibleStatus(in.Visible))
	if err != nil {
		return &pb.SetVisibleStatusPostResponse{
			Status:  constants.STATUS_RESPONSE_NOT_OK,
			Message: "fail to set visible status",
		}, nil
	}
	return nil, nil
}
func (s *Server) DeletePost(ctx context.Context, in *pb.DeletePostRequest) (*pb.DeletePostResponse, error) {
	err := s.store.DeletePost(in.UserId, in.PostId)
	if err != nil {
		return &pb.DeletePostResponse{
			Status:  constants.STATUS_RESPONSE_NOT_OK,
			Message: "fail to delete post",
		}, nil
	}
	return &pb.DeletePostResponse{
		Status:  constants.STATUS_RESPONSE_OK,
		Message: "success",
	}, nil
}
