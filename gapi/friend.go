package gapi

import (
	"context"
	"log"

	"github.com/thnam4500/identity/constants"
	"github.com/thnam4500/identity/pb"
)

func (s *Server) FollowFriendList(ctx context.Context, req *pb.FollowFriendListRequest) (*pb.FollowFriendListResponse, error) {
	result, err := s.store.GetFollowedUser(req.UserId, req.Page, req.Limit)
	if err != nil {
		log.Println(err)
		return &pb.FollowFriendListResponse{
			Status:  constants.STATUS_RESPONSE_NOT_OK,
			Message: "failed to get followed user",
			Data:    []*pb.UserData{},
		}, err
	}
	data := []*pb.UserData{}
	for _, v := range result {
		data = append(data, v.ToGetUserProtoResponse())
	}
	return &pb.FollowFriendListResponse{
		Status:  constants.STATUS_RESPONSE_OK,
		Message: "success",
		Data:    data,
	}, nil
}
func (s *Server) FollowFriend(ctx context.Context, req *pb.FollowFriendRequest) (*pb.FollowFriendResponse, error) {
	err := s.store.FollowUser(req.UserId, req.FriendId)
	if err != nil {
		return &pb.FollowFriendResponse{
			Status:  constants.STATUS_RESPONSE_NOT_OK,
			Message: "fail",
		}, nil
	}
	return &pb.FollowFriendResponse{
		Status:  constants.STATUS_RESPONSE_OK,
		Message: "success",
	}, nil
}
func (s *Server) UnFollowFriend(ctx context.Context, req *pb.UnFollowFriendRequest) (*pb.UnFollowFriendResponse, error) {
	err := s.store.UnFollowUser(req.UserId, req.FriendId)
	if err != nil {
		return &pb.UnFollowFriendResponse{
			Status:  constants.STATUS_RESPONSE_NOT_OK,
			Message: "fail",
		}, nil
	}
	return &pb.UnFollowFriendResponse{
		Status:  constants.STATUS_RESPONSE_OK,
		Message: "success",
	}, nil
}
