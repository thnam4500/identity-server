package gapi

import (
	"context"

	"github.com/thnam4500/identity/constants"
	"github.com/thnam4500/identity/pb"
	"golang.org/x/crypto/bcrypt"
)

func (s *Server) Login(ctx context.Context, req *pb.LoginRequest) (*pb.LoginResponse, error) {
	return &pb.LoginResponse{
		Status:  constants.STATUS_RESPONSE_OK,
		Message: "success",
		Token:   "signedToken",
	}, nil
}

func checkPassword(hashedPassword, password string) bool {
	err := bcrypt.CompareHashAndPassword([]byte(hashedPassword), []byte(password))
	return err == nil
}
