package gapi

import (
	"context"
	"fmt"
	"time"

	"github.com/thnam4500/identity/constants"
	"github.com/thnam4500/identity/models"
	"github.com/thnam4500/identity/pb"
	"github.com/thnam4500/identity/store"
	"golang.org/x/crypto/bcrypt"
)

func (s *Server) SignUp(ctx context.Context, req *pb.SignUpRequest) (*pb.SignUpResponse, error) {
	params := store.GetUserParams{
		Username: req.Username,
	}
	_, exist, err := s.store.GetUserByCondition(params)
	if err != nil {
		return &pb.SignUpResponse{
			Status:  constants.STATUS_RESPONSE_NOT_OK,
			Message: "failed to get user",
		}, err
	}
	if exist {
		return &pb.SignUpResponse{
			Status:  constants.STATUS_RESPONSE_NOT_OK,
			Message: "username already exist",
		}, nil
	}
	hashedPassword, err := hashPassword(req.GetPassword())
	if err != nil {
		return &pb.SignUpResponse{
			Status:  constants.STATUS_RESPONSE_NOT_OK,
			Message: "failed to hash password",
		}, err
	}
	user := &models.User{
		Username:       req.GetUsername(),
		Email:          req.GetEmail(),
		HashedPassword: hashedPassword,
		DateOfBirth:    req.GetBirthday(),
		FirstName:      req.GetFirstName(),
		LastName:       req.GetLastName(),
		Role:           int32(models.UserRole),
		CreatedAt:      time.Now().Unix(),
		UpdatedAt:      time.Now().Unix(),
	}
	err = s.store.CreateUser(user)
	if err != nil {
		return &pb.SignUpResponse{
			Status:  constants.STATUS_RESPONSE_NOT_OK,
			Message: "fail to create user",
		}, err
	}
	return &pb.SignUpResponse{
		Status:  constants.STATUS_RESPONSE_OK,
		Message: "success",
	}, nil
}

func hashPassword(password string) (string, error) {
	hashedPassword, err := bcrypt.GenerateFromPassword([]byte(password), bcrypt.DefaultCost)
	if err != nil {
		fmt.Println("Error generating hashed password:", err)
		return "", err
	}
	return string(hashedPassword), nil
}
